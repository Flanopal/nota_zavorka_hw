local sensorInfo = {
	name = "Get base positions",
	desc = "Get evac position on base",
	author = "Flanopal",
	date = "2019-05-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @get positions
return function(count, baseCenter, radius)
	
	local basePositions = {}
	local startDif = radius / math.sqrt(2)
	local evacStart = Vec3(baseCenter.x - startDif,baseCenter.y,baseCenter.z - startDif)
	local width = math.ceil(math.sqrt(count))
	local dif = (startDif + startDif) / width
	local evacPoint = Vec3(evacStart.x,evacStart.y,evacStart.z)
	local index = 1

	-- place all evacuation points into square in base radius
	for i=1, count do
		basePositions[i] = Vec3(evacPoint.x,evacPoint.y,evacPoint.z)
		index = index + 1
		if index > width then
			index = 1
			evacStart.z = evacStart.z + dif
			evacPoint = Vec3(evacStart.x,evacStart.y,evacStart.z)
		else
			evacPoint.x = evacPoint.x + dif
		end
	end

	return basePositions
end