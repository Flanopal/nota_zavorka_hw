local sensorInfo = {
	name = "Reverse movement plan",
	desc = "Reverse movement plan",
	author = "Flanopal",
	date = "2019-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @reverse plan
return function(plan)

	-- starts from 2, because first element of plan is index of currently visiting position
	for i=2,#plan/2 + 1 do
		local point = plan[i]
		plan[i] = plan[#plan - i + 2]
		plan[#plan - i + 2] = point
	end

	return plan
end