local sensorInfo = {
	name = "Send formation positions",
	desc = "Set formation positions",
	author = "Flanopal",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @send units
return function(count,point,dx,dz)
	local j=1
	local positions = {}
	for i = 1, count do
		local target = Vec3(point.x,point.y,point.z)
		if j==2 then
			target.x=target.x+dz
			target.z=target.z-dx
		elseif j==3 then
			target.x=target.x-dz
			target.z=target.z+dx
		end
		positions[i] = Vec3(target.x,target.y,target.z)
		j=j+1
		if j==4 then
			point.x=point.x-dx
			point.z=point.z-dz
			j=1
		end
	end
	return positions
end